#!/bin/bash

plasma_store -m 1000000000 -s /tmp/plasma &
gunicorn --workers=1 --threads=4 -b 0.0.0.0:8000 --timeout=600 app:server
